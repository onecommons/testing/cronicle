# Application Blueprint for Cronicle

## What is Cronicle?
A simple, distributed task scheduler and runner with a web based UI.

**Credentials:** `admin/admin`

Taken from [https://github.com/jhuckaby/Cronicle](https://github.com/jhuckaby/Cronicle) and [https://github.com/soulteary/docker-cronicle](https://github.com/soulteary/docker-cronicle)

This Application Blueprint is made possible by the [Community Maintained One Click Apps repository](https://github.com/caprover/one-click-apps).
If there are any apps you would like to help find their place in our Free and Open Cloud, consider contributing to One-Click Apps as well.
For more information on CapRover or their one One-Click Apps platform, visit [caprover.com](https://caprover.com/docs/one-click-apps.html).

`ensemble-generated.yaml` was generated from [https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/cronicle.yml](https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/cronicle.yml).  Changes can instead be applied to `ensemble-template.yaml` to overwrite the generated values.
